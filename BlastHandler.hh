#ifndef BLASTHANDLER_HH
#define BLASTHANDLER_HH
#include "timer.hh"
#include <string>
#include <map>
#include <fstream>
#include <vector>
#include <memory>

#define MAKEBLASTDB "include/ncbi-blast+/bin/makeblastdb"
#define BLASTN "include/ncbi-blast+/bin/blastn"
#define TBLASTN "include/ncbi-blast+/bin/tblastn"
#define BLASTP "include/ncbi-blast+/bin/blastp"
#define BLASTX "include/ncbi-blast+/bin/blastx"
#define BLASTDBCMD "include/ncbi-blast+/bin/blastdbcmd"

class BlastHandler
{
public:
    BlastHandler(std::map<std::string, std::string>& arguments,
                 std::map<std::string, bool>& flags, std::shared_ptr<Timer> timer);
    void runBlast();
    void setQueryFilePath(std::string& file_path);
    std::string getQueryFilePath() const;
    void setDBFilePath(std::string& file_path);
    std::string getDBFilePath() const;
    void setBlastType(std::string& blast_type);
    std::string getBlastType() const;
    void setEvalue(std::string& evalue);
    std::string getEvalue() const;
    void setLimit(std::string& limit);
    std::string getLimit() const;
    void setVerboseFlag(bool& flag);
    bool getVerboseFlag() const;
    void setTimedFlag(bool& flag);
    bool getTimedFlag() const;
    void setBlastDBTypes(std::map<std::string, std::string>& blast_types);
    std::map<std::string, std::string> getBlastDBTypes() const;
    void setOutputFilePath(std::string file_path);
    std::string getOutputFilePath() const;
    void setTempResultsFilePath(std::string file_path);
    std::string getTempResultsFilePath() const;
    void setNCBIBlastStdoutFilePath(std::string file_path);
    std::string getNCBIBlastStdoutFilePath() const;
    void setNCBIBlastStderrFile(std::string file_path);
    std::string getNCBIBlastStderrFilePath() const;


private:
    bool isValidBlastDB() const;
    bool createBlastDB(std::string& DB_type);
    void processBlastOutput10();
    bool readFullSequencesFromBlastDB(std::string& output_file);
    void readFullSequencesFromDBFastaFile(std::vector<std::string>& seqids,
                                          std::string& output_file);
    void verbosePrinting(std::string message) const;

    std::string query_fp_;
    std::string db_fp_;
    std::string blast_type_;
    std::string evalue_;
    std::string limit_;
    bool verbose_;
    bool timed_;

    std::map<std::string, std::string> blast_db_types_ = {{"blastn", "nucl"},
                                                          {"tblasn", "nucl"},
                                                          {"blastp", "prot"},
                                                          {"blastx", "prot"}
                                                           };
    std::string blast_out_fp_ = "output/blast_out";
    std::string temp_results_fp_ = "output/temp";
    std::string ncbi_stdout_fp_ = "output/ncbi_blast_stdout";
    std::string ncbi_stderr_fp_ = "output/ncbi_blast_stderr";

    int num_threads_;
    std::shared_ptr<Timer> timer_p_ = nullptr;

};

#endif // BLASTEXECUTABLESAPI_HH
