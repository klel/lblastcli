#include "argumentvalidator.hh"
#include "fileutils.hh"
#include "programexception.hh"
#include <iostream>
#include <fstream>
#include <set>
#include <algorithm>

ArgumentValidator::ArgumentValidator()
{

}

void ArgumentValidator::validateArguments(std::map<std::string, std::string>
                                          &arguments)
{
    // check file access for both q_file and dbs_file
    FileUtils::checkFileAccess(arguments["q_file"]);
    FileUtils::checkFileAccess(arguments["dbs_file"]);

    // check blast_type is valid
    std::string bt_arg = arguments["blast_type"];
    if (!isValidBlastType(bt_arg))
    {
        throw ProgramException("blast_type " + bt_arg + " is not valid. Please use "
                                                          "blastn, tblastn, blastp"
                                                        "or blastx.");
    }

    // check if limit is a valid unsigned
    try
    {
        std::stoul(arguments["limit"]);
    }
    catch (const std::invalid_argument& ia)
    {
        throw ProgramException(std::string(ia.what()) + " for argument limit."
                                                        "\nUse an unsigned "
                                         "integer value for option --limit.");
    }

    // check if evalue is a valid probability
    try
    {
        if (!isValidProbability(arguments["evalue"]))
        {
            throw std::domain_error("invalid probability");
        }
    }
    catch(std::exception& e)
    {
        std::cout << "invalid argument for evalue: " << arguments["evalue"] <<
                     "\nPlease use a number between (0,1)."
                  << std::endl;
    }

}

std::vector<std::string> ArgumentValidator::getBlastTypes() const
{
    return blast_types_;
}

void ArgumentValidator::setBlastTypes(std::vector<std::string> &blast_types)
{
    blast_types_ = blast_types;
}

bool ArgumentValidator::isValidProbability(std::string& p_str)
{
    float p = std::stof(p_str);
    if (p >= 1 || p <= 0)
    {
        return false;
    }
    return true;
}

bool ArgumentValidator::isValidBlastType(std::string &blast_type)
{
    if(std::find(blast_types_.begin(), blast_types_.end(), blast_type)
            == blast_types_.end())
    {
        return false;
    }
    return true;
}
