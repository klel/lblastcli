#include "fileutils.hh"
#include "programexception.hh"
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <set>

void FileUtils::checkFileAccess(std::string& path)
{
    std::ifstream fs;
    fs.open(path);
    if(!fs)
    {
        throw ProgramException("Could not access file: " + path);
    }
    fs.close();
}

std::string FileUtils::getFileNameFromPath(std::string &path)
{
    std::size_t start = path.find_last_of("/");
    if (start == std::string::npos)
    {
        start = 0;
    }
    std::size_t end = path.find_last_of(".");
    if (end != std::string::npos)
    {
        end = end - start;
    }
    return path.substr(start+1, end-1);
}
