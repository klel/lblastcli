#ifndef TIMER_HH
#define TIMER_HH
#include <chrono>
#include <string>

class Timer
{
public:
    Timer();
    void start();
    double getDurationFromStart() const;
    void logEventDuration(std::__cxx11::string event_message);
    void setLogFlag(bool flag);
    bool getLogFlag() const;

private:
    std::chrono::high_resolution_clock::time_point start_;
    std::string log_fp_ = "time_log";
    bool log_flag_;
};

#endif // TIMER_HH
