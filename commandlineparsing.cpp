#include "commandlineparsing.hh"
#include "include/cxxopts/cxxopts.hpp"
#include "programexception.hh"
#include <set>


void CommandLineParsing::parseOptions(int argc, char *argv[],
                                      std::map<std::string, std::string>
                                      &parsed_arguments,
                                      std::map<std::string, bool> &parsed_flags)
{
    try
    {
        cxxopts::Options options("LBlastCLI", "A CLI for running blastn, tblastn,"
                                              "blastp or blastx using local sequence"
                                              " files or blast databases as search"
                                              " targets.");
        options.add_options()
                ("h, help", "Help message")
                ("v, verbose", "Turn on printing", cxxopts::value<bool>
                 (parsed_flags["verbose"]))
                ("t, timed", "Turn on time logging", cxxopts::value<bool>
                 (parsed_flags["timed"]))
                ("q_file", "FASTA file with query sequences",
                 cxxopts::value<std::string>(parsed_arguments["q_file"]))
                ("dbs_file", "Text file with a FASTA database file per line",
                 cxxopts::value<std::string>(parsed_arguments["dbs_file"]))
                ("blast_type", "Blast type: blastn, tblastn, blastp, blastx",
                 cxxopts::value<std::string>(parsed_arguments["blast_type"])->
                default_value("blastn"))
                ("evalue", "Evalue cutoff",
                 cxxopts::value<std::string>(parsed_arguments["evalue"])->
                default_value("0.001"))
                ("limit", "Max limit of sequences includes in a result file",
                 cxxopts::value<std::string>(parsed_arguments["limit"])->
                default_value(("100")));

        auto parsed_options = options.parse(argc, argv);

        std::set<std::string> required_options = {"q_file", "dbs_file"};
        if (parsed_options.count("help") != 0)
        {
                std::cout << options.help() << std::endl;
                throw ProgramException("");
        }

        // check that all required arguments are present
        for (std::string option : required_options)
        {
            if (!(parsed_options.count(option)))
            {
                throw ProgramException("missing required option " + option);
            }
        }
    }
    catch (const cxxopts::OptionException& e)
    {
        std::cout << "error parsing options: " << e.what() << std::endl;
    }
    catch (const std::exception& e)
    {
        std::cout << e.what() << std::endl;
    }
}
