#ifndef COMMANDLINEPARSING_HH
#define COMMANDLINEPARSING_HH
#include <map>
#include <string>

namespace CommandLineParsing
{
    void parseOptions(int argc, char *argv[],
                      std::map<std::string, std::string>& parsed_arguments,
                      std::map<std::string, bool> &parsed_flags);
}

#endif // COMMANDLINEPARSING_HH
