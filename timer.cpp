#include "timer.hh"
#include <fstream>
#include <ctime>
#include <ratio>

Timer::Timer()
{

}

void Timer::start()
{
    start_ = std::chrono::high_resolution_clock::now();
}

double Timer::getDurationFromStart() const
{
    auto now = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> time_span =
            std::chrono::duration_cast<std::chrono::duration<double>>(now-start_);
    return time_span.count();
}

void Timer::logEventDuration(std::string event_message)
{
    if(log_flag_)
    {
        std::ofstream log;
        log.open(log_fp_, std::ios::app);
        log << event_message << " : " << getDurationFromStart() << "\n";
    }

}

void Timer::setLogFlag(bool flag)
{
    log_flag_ = flag;
}

bool Timer::getLogFlag() const
{
    return log_flag_;
}
