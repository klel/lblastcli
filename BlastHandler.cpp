#include "BlastHandler.hh"
#include "fileutils.hh"
#include "timer.hh"
#include <iostream>
#include <cstring>
#include <set>
#include <thread>
#include <algorithm>

BlastHandler::BlastHandler(std::map<std::string, std::string> &arguments,
                           std::map<std::string, bool> &flags,
                           std::shared_ptr<Timer> timer) :
    query_fp_(arguments["q_file"]),
    db_fp_(arguments["dbs_file"]),
    blast_type_(arguments["blast_type"]),
    evalue_(arguments["evalue"]),
    limit_(arguments["limit"]),
    verbose_(flags["verbose"]),
    timed_(flags["timed"])
{
    num_threads_ = std::max(uint(1), std::thread::hardware_concurrency());
    timer_p_ = timer;
}

void BlastHandler::runBlast()
{
    // check DB is blastdb type, if not, create
    if (!isValidBlastDB())
    {
        std::string DB_type = blast_db_types_[blast_type_];
        if (!createBlastDB(DB_type))
        {
            std::cout << "Error in Blast DB creation for " << db_fp_ <<std::endl;
            return;
        }
    }

    verbosePrinting("Running " + blast_type_ + " for query file " + query_fp_
                    + " against database file " + db_fp_);

    std::string blast_exe_str = std::string(BLASTN);

    if (blast_type_ == "blastn")
    {
        blast_exe_str = std::string(BLASTN);
    }
    else if (blast_type_ == "tblastn")
    {
        blast_exe_str = std::string(TBLASTN);
    }
    else if (blast_type_ == "blastp")
    {
        blast_exe_str = std::string(BLASTP);
    }
    else if (blast_type_ == "blastx")
    {
        blast_exe_str = std::string(BLASTX);
    }
    else
    {
        // error, shouldn't happen if argument parsing worked correctly
    }

    std::string command = blast_exe_str + " -query " + query_fp_ +
                            " -db " + db_fp_ + " -outfmt \"10 qacc sacc\" -out " +
                            blast_out_fp_
                            + " >> " + ncbi_stdout_fp_ + " 2>> " + ncbi_stderr_fp_;
    const char* c_command = command.c_str();
    if (timed_) {timer_p_->start();}
    int exit_code = system(c_command);
    if (timed_) {timer_p_->logEventDuration("blasting");}
    if (WEXITSTATUS(exit_code) != 0)
    {
        // if getting error from NCBI Blast+, let user know
        std::string err_msg = "Error with blastn with query file " +
                query_fp_ + " with database " + db_fp_ + ". See file " +
                ncbi_stderr_fp_ + " for details";
        std::cout << err_msg << std::endl;
        return;
    }

    // process blast output
    processBlastOutput10();
    verbosePrinting("Results ready for database " + db_fp_ + "\n");
}

void BlastHandler::setQueryFilePath(std::string &file_path)
{
    query_fp_ = file_path;
}

std::string BlastHandler::getQueryFilePath() const
{
    return query_fp_;
}

void BlastHandler::setDBFilePath(std::string &file_path)
{
    db_fp_ = file_path;
}

std::string BlastHandler::getDBFilePath() const
{
    return db_fp_;
}

void BlastHandler::setBlastType(std::string &blast_type)
{
    blast_type_ = blast_type;
}

std::string BlastHandler::getBlastType() const
{
    return blast_type_;
}

void BlastHandler::setEvalue(std::string &evalue)
{
    evalue_ = evalue;
}

std::string BlastHandler::getEvalue() const
{
    return evalue_;
}

void BlastHandler::setLimit(std::string &limit)
{
    limit_ = limit;
}

std::string BlastHandler::getLimit() const
{
    return limit_;
}

void BlastHandler::setVerboseFlag(bool &flag)
{
    verbose_ = flag;
}

bool BlastHandler::getVerboseFlag() const
{
    return verbose_;
}

void BlastHandler::setTimedFlag(bool &flag)
{
    timed_ = flag;
}

bool BlastHandler::getTimedFlag() const
{
    return timed_;
}

void BlastHandler::setBlastDBTypes(std::map<std::string, std::string> &blast_types)
{
    blast_db_types_ = blast_types;
}

std::map<std::string, std::string> BlastHandler::getBlastDBTypes() const
{
    return blast_db_types_;
}

void BlastHandler::setOutputFilePath(std::string file_path)
{
    blast_out_fp_ = file_path;
}

std::string BlastHandler::getOutputFilePath() const
{
    return blast_out_fp_;
}

void BlastHandler::setTempResultsFilePath(std::string file_path)
{
    temp_results_fp_ = file_path;
}

std::string BlastHandler::getTempResultsFilePath() const
{
    return temp_results_fp_;
}

void BlastHandler::setNCBIBlastStdoutFilePath(std::string file_path)
{
    ncbi_stdout_fp_ = file_path;
}

std::string BlastHandler::getNCBIBlastStdoutFilePath() const
{
    return ncbi_stdout_fp_;
}

void BlastHandler::setNCBIBlastStderrFile(std::string file_path)
{
    ncbi_stderr_fp_ = file_path;
}

std::string BlastHandler::getNCBIBlastStderrFilePath() const
{
    return ncbi_stderr_fp_;
}

// PRIVATE

bool BlastHandler::isValidBlastDB() const
{
    std::string command = std::string(BLASTDBCMD) + " -db " + db_fp_ +
                            " -info" +
                            " >> " + ncbi_stdout_fp_ + " 2>> " + ncbi_stderr_fp_;
    verbosePrinting("Checking if " + db_fp_ + " is a valid Blast DB");
    const char* c_command = command.c_str();
    int exit_code = system(c_command);
    if (WEXITSTATUS(exit_code) != 0)
    {
        return false;
    }
    return true;
}


bool BlastHandler::createBlastDB(std::string &DB_type)
{
    std::string command = std::string(MAKEBLASTDB) + " -in " + db_fp_ +
                          " -dbtype " + DB_type + " -parse_seqids" +
                          " >> " + ncbi_stdout_fp_ + " 2>> " + ncbi_stderr_fp_;
    verbosePrinting("Creating Blast DB for " + db_fp_);
    const char* c_command = command.c_str();
    int exit_code = system(c_command);
    if (WEXITSTATUS(exit_code) != 0)
    {
        return false;

    }
    return true;
}

void BlastHandler::processBlastOutput10()
{
    verbosePrinting("Processing blast output for DB " + db_fp_);
    std::ifstream blast_output(blast_out_fp_);
    std::string query_subjects_line;
    std::map<std::string, std::set<std::string>> query_subjects_map;

    // create map with query sequences & sets of their hits (db accession ids)
    while (std::getline(blast_output, query_subjects_line))
    {
        size_t delimiter_pos = query_subjects_line.find(",");
        std::string query = query_subjects_line.substr(0, delimiter_pos);
        std::string subject = query_subjects_line.substr(delimiter_pos+1);
        (query_subjects_map[query]).insert(subject);
    }

    // write temp text file for each query's subject hits and get their full sequences
    for (auto pair : query_subjects_map)
    {
        std::ofstream seqids_file(temp_results_fp_);
        std::vector<std::string> seqids_vec;
        for (auto subject : pair.second)
        {
            seqids_file << subject << std::endl;
            seqids_vec.push_back(subject);
        }
        seqids_file.close();

        // results text file named by format SEQUENCE-ACC_DATABASE-NAME_results
        std::string results_file_name = pair.first + "_" +
                                        FileUtils::getFileNameFromPath(db_fp_)
                                        + "_results";

        // attempt to get full sequences from DB and write to results file
        if (timed_) {timer_p_->start();}
//        int exit_code = readFullSequencesFromBlastDB(results_file_name);
        if (timed_) {timer_p_->logEventDuration("full seq retrieval from DB");}
        if (!readFullSequencesFromBlastDB(results_file_name))
        {
            // if getting error, search through original FASTA text file instead,
            verbosePrinting("Unable to retrieve full sequences from Blast "
                            "DB " + db_fp_);
            if (timed_) {timer_p_->start();}
            readFullSequencesFromDBFastaFile(seqids_vec, results_file_name);
            if (timed_) {timer_p_->logEventDuration("full seq retrieval from FASTA");}
        }
        else
        {
             if(timed_) {timer_p_->logEventDuration("full seq retrieval from DB");}
        }
    }
}

bool BlastHandler::readFullSequencesFromBlastDB(std::string& output_file)
{
    std::string command = std::string(BLASTDBCMD) + " -db " + db_fp_ +
                            " -entry_batch " + temp_results_fp_ + " -out " +
                            output_file + " -outfmt %f" +
                            " >> " + ncbi_stdout_fp_ + " 2>> " + ncbi_stderr_fp_;
    verbosePrinting("Retrieving full sequences from Blast DB " + db_fp_);
    const char* c_command = command.c_str();
    int exit_code = system(c_command);
    if (WEXITSTATUS(exit_code) != 0)
    {
        return false;
    }
    return true;
}


void BlastHandler::readFullSequencesFromDBFastaFile(std::vector<std::string>& seqids,
                                                    std::string& output_file)
{
    verbosePrinting("Retrieving full sequences from original FASTA file "
                    + db_fp_);
    std::ifstream db_file(db_fp_);
    std::ofstream output(output_file);

    std::string line;
    std::size_t seq_pos;
    bool copy_flag = false;

    while(std::getline(db_file, line))
    {
        seq_pos = line.find(">");

        // if the startline of a sequence
        if (seq_pos != std::string::npos)
        {
            for (auto it = seqids.begin(); it != seqids.end(); ++it)
            {
                if (line.find(*it) != std::string::npos)
                {
                    copy_flag = true;
                    output << line << std::endl;
                    break;
                }
            }
        }
        // if another line
        else if(copy_flag)
        {
            output << line << std::endl;
        }
    }
}


void BlastHandler::verbosePrinting(std::string message) const
{
    if(verbose_)
    {
        std::cout << message << std::endl;
    }
}
