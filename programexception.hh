#ifndef PROGRAMEXCEPTION_HH
#define PROGRAMEXCEPTION_HH
#include <string>
#include <stdexcept>
#include <exception>

// A ProgramException class from which all Exception classes should be inherited.
class ProgramException : public std::exception
{
public:
    explicit ProgramException(const std::string& message);
    virtual const char* what() const throw();

protected:
    std::string msg_;

};

#endif // PROGRAMEXCEPTION_HH
