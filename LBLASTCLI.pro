TEMPLATE = app
CONFIG += console c++11
#CONFIG -= app_bundle
CONFIG += qt
QT += core

SOURCES += \
        main.cpp \
    programexception.cpp \
    BlastHandler.cpp \
    argumentvalidator.cpp \
    fileutils.cpp \
    commandlineparsing.cpp \
    timer.cpp

HEADERS += \
    programexception.hh \
    BlastHandler.hh \
    argumentvalidator.hh \
    fileutils.hh \
    commandlineparsing.hh \
    timer.hh


RESOURCES += \
    blast_exes.qrc
