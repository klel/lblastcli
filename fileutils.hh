#include <string>
#include <map>

namespace FileUtils
{
    void checkFileAccess(std::string& path);
    std::string getFileNameFromPath(std::string& path);
}

