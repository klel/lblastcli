#include "programexception.hh"

ProgramException::ProgramException(const std::string &message):
msg_(message)
{

}

const char *ProgramException::what() const throw()
{
    return msg_.c_str();
}

