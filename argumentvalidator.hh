#ifndef ARGUMENTVALIDATOR_HH
#define ARGUMENTVALIDATOR_HH
#include <map>
#include <vector>


class ArgumentValidator
{
public:
    ArgumentValidator();
    void validateArguments(std::map<std::string, std::string>& arguments);
    std::vector<std::string> getBlastTypes() const;
    void setBlastTypes(std::vector<std::string>& blast_types);

private:
    bool isValidProbability(std::string& p_str);
    bool isValidBlastType(std::string& blast_type);

    std::vector<std::string> blast_types_  {std::vector<std::string>{"blastn",
                                                                     "tblastn",
                                                                     "blastp",
                                                                     "blastx"}};
};

#endif // ARGUMENTVALIDATOR_HH
