#include "fileutils.hh"
#include "commandlineparsing.hh"
#include "BlastHandler.hh"
#include "argumentvalidator.hh"
#include "timer.hh"
#include <map>
#include <string>
#include <fstream>
#include <iostream>


int main(int argc, char *argv[])
{
    // Timer for benchamrking different parts of the program
    std::shared_ptr<Timer> timer = std::shared_ptr<Timer>(new Timer);

    // parse command line options and arguments
    std::map<std::string, std::string> parsed_arguments;
    std::map<std::string, bool> parsed_flags;

    try
    {        
        timer->start();
        CommandLineParsing::parseOptions(argc, argv, parsed_arguments, parsed_flags);

        timer->setLogFlag(parsed_flags["timed"]);

        timer->logEventDuration("argument parsing");

        // check validity of each argument
        ArgumentValidator validator;
        timer->start();
        validator.validateArguments(parsed_arguments);
        timer->logEventDuration("argument validation");
    }
    catch (std::exception& e)
    {
        std::cout << e.what() << std::endl;
        return 1;
    }

    // pass arguments, flags and Timer pointer to BlastHandler
    BlastHandler handler(parsed_arguments, parsed_flags, timer);

    // check path on line in dbs_file
    std::ifstream dbs_file(parsed_arguments["dbs_file"]);
    std::string db_path;
    while (std::getline(dbs_file, db_path))
    {
        try
        {
            FileUtils::checkFileAccess(db_path);
            handler.setDBFilePath(db_path);
            handler.runBlast();
        }
        catch (std::exception& e)
        {
            std::cout << e.what() << std::endl;
        }
    }
    

    return 0;
}
